import sys
import matplotlib.pyplot as plt

y = []

maxy = 0.0

for line in sys.stdin:
  times = line.split ()
  if len (y) == 0:
    for t in times:
      y.append ([float(t)])
  else:
    i = 0
    for t in times:
      y[i].append (float(t))
      maxy = max (maxy, float(t))
      i+=1


k = len(y[0])
x = [2**i for i in range(10, 10 + k)]

colors = ['g-', 'r--', 'b--', 'y--']
labels = ['bubble_sort', 'selection_sort', 'insertion_sort', 'insertion_sort_bin']
i = 0
for y in y:
  plt.plot (x, y, colors[i], label=labels[i])
  i += 1

plt.xlabel('n')
plt.ylabel('time')
plt.title('Stupid sorts. O(n^2)')
plt.axis([0, x[-1]*1.05, 0, maxy])
plt.legend(loc='upper left')
plt.grid(True)
plt.show ()