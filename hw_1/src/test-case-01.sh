#!/bin/bash

mkdir -p tests01
echo "Generating test #1"
files/gen-input-tests.sh  "files/gen1 1024" "tests01/01" 1
echo "Generating test #2"
files/gen-input-tests.sh  "files/gen1 2048" "tests01/02" 2
echo "Generating test #3"
files/gen-input-tests.sh  "files/gen1 4096" "tests01/03" 3
echo "Generating test #4"
files/gen-input-tests.sh  "files/gen1 8192" "tests01/04" 4
echo "Generating test #5"
files/gen-input-tests.sh  "files/gen1 16384" "tests01/05" 5
echo "Generating test #6"
files/gen-input-tests.sh  "files/gen1 32768" "tests01/06" 6
echo "Generating test #7"
files/gen-input-tests.sh  "files/gen1 65536" "tests01/07" 7
echo "Generating test #8"
files/gen-input-tests.sh  "files/gen1 131072" "tests01/08" 8
echo "Generating test #9"
files/gen-input-tests.sh  "files/gen1 262144" "tests01/09" 9
echo "Generating test #10"
files/gen-input-tests.sh  "files/gen1 524288" "tests01/10" 10
echo "Generating test #11"
files/gen-input-tests.sh  "files/gen1 1048576" "tests01/11" 11