#!/bin/bash

files=`find $1/ -type f -regex '.*[0-9]$' | sort`

echo >$2
for in in $files
do
  ./sorts <$in >>$2
done