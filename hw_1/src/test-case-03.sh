#!/bin/bash

mkdir -p tests03
echo "Generating test #1"
files/gen-input-tests.sh  "files/gen3 1024 100" "tests03/01" 1
echo "Generating test #2"
files/gen-input-tests.sh  "files/gen3 2048 100" "tests03/02" 2
echo "Generating test #3"
files/gen-input-tests.sh  "files/gen3 4096 100" "tests03/03" 3
echo "Generating test #4"
files/gen-input-tests.sh  "files/gen3 8192 1000" "tests03/04" 4
echo "Generating test #5"
files/gen-input-tests.sh  "files/gen3 16384 1000" "tests03/05" 5
echo "Generating test #6"
files/gen-input-tests.sh  "files/gen3 32768 1000" "tests03/06" 6
echo "Generating test #7"
files/gen-input-tests.sh  "files/gen3 65536 1000" "tests03/07" 7
echo "Generating test #8"
files/gen-input-tests.sh  "files/gen3 131072 1000" "tests03/08" 8
echo "Generating test #9"
files/gen-input-tests.sh  "files/gen3 262144 1000" "tests03/09" 9
echo "Generating test #10"
files/gen-input-tests.sh  "files/gen3 524288 1000" "tests03/10" 10
echo "Generating test #11"
files/gen-input-tests.sh  "files/gen3 1048576 1000" "tests03/11" 11