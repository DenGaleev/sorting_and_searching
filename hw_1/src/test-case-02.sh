#!/bin/bash

mkdir -p tests02
echo "Generating test #1"
files/gen-input-tests.sh  "files/gen2 1024 100" "tests02/01" 1
echo "Generating test #2"
files/gen-input-tests.sh  "files/gen2 2048 100" "tests02/02" 2
echo "Generating test #3"
files/gen-input-tests.sh  "files/gen2 4096 100" "tests02/03" 3
echo "Generating test #4"
files/gen-input-tests.sh  "files/gen2 8192 1000" "tests02/04" 4
echo "Generating test #5"
files/gen-input-tests.sh  "files/gen2 16384 1000" "tests02/05" 5
echo "Generating test #6"
files/gen-input-tests.sh  "files/gen2 32768 1000" "tests02/06" 6
echo "Generating test #7"
files/gen-input-tests.sh  "files/gen2 65536 1000" "tests02/07" 7
echo "Generating test #8"
files/gen-input-tests.sh  "files/gen2 131072 1000" "tests02/08" 8
echo "Generating test #9"
files/gen-input-tests.sh  "files/gen2 262144 1000" "tests02/09" 9
echo "Generating test #10"
files/gen-input-tests.sh  "files/gen2 524288 1000" "tests02/10" 10
echo "Generating test #11"
files/gen-input-tests.sh  "files/gen2 1048576 1000" "tests02/11" 11