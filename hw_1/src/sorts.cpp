#include <bits/stdc++.h>

using namespace std;

template<class T> 
void bubble_sort (T arr[], size_t size) {
  for (size_t i = 0; i < size; ++i) {
    for (size_t j = 0; j < size - i - 1; ++j) {
      if (arr[j + 1] < arr[j]) {
        swap (arr[j], arr[j + 1]);
      }
    }
  }
}

template<class T> 
void selection_sort (T arr[], size_t size) {
  for (size_t i = 0; i < size; ++i) {
    size_t min_id = i; 
    for (size_t j = i; j < size; ++j) {
      if (arr[j] < arr[min_id]) {
        min_id = j;      
      }
    }

    if (i != min_id) {
      swap (arr[i], arr[min_id]);
    }
  }
}

template<class T> 
void insertion_sort (T arr[], size_t size) {
  for (size_t i = 1; i < size; ++i) {
    size_t j = i; 
    while (j > 0 && (arr[j] < arr[j - 1])) {
      swap (arr[j], arr[j - 1]); 
      --j; 
    }
  }
}

template<class T> 
void insertion_sort_bin (T arr[], size_t size) {
  for (size_t i = 1; i < size; ++i) {
    size_t l, r, m; 
    l = 0; r = i;
    while (l < r) {
      m = (l + r) / 2;
      if (arr[m] < arr[i]) {
        l = m + 1;
      } else {
        r = m;
      }
    }

    size_t j = i; 
    while (j != l) {
      swap (arr[j], arr[j - 1]);
      --j;
    }
  }
}

  
const int MAXN = 2e+6; 
int a[MAXN]; 
int b[MAXN];
size_t n;

template<class T> 
void run (void (*mysort)(T arr[], size_t size)) {
  memcpy (b, a, sizeof (*a) * n);
  clock_t t;
  t = clock();

  mysort (b, n);

  t = clock() - t;
  printf ("%f ", ((double)t)/CLOCKS_PER_SEC);
}


int main () {
  scanf ("%lu", &n);
  for (size_t i = 0; i < n; ++i) {
    scanf ("%d", &a[i]);
  }

  run (bubble_sort<int>);
  run (selection_sort<int>);
  run (insertion_sort<int>);
  run (insertion_sort_bin<int>);
  
  printf("\n");

  return 0;
} 