#include "testlib.h"
#include <iostream>
#include <string>

using namespace std;


const int MAXA = 1e9; 

int main(int argc, char* argv[]) {
  registerGen(argc, argv, 1);

  int n, m;
  n = atoi(argv[1]);
  m = atoi(argv[2]);

  vector<int> v (n);
  
  int t = 0;
  for (auto &vi: v) {
    vi = ++t; 
  } 

  sort (v.begin (), v.end ());
  for (size_t i = 0; i < m; ++i) {
    size_t j, k;
    j = rnd.next (0, n - 1);
    k = rnd.next (0, n - 1);
    swap (v[j], v[k]);
  }

  cout << n << "\n"; 
  for (auto &vi: v) {
    cout << vi << " ";
  }

  return 0;
}