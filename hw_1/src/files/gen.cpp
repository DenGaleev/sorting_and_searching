#include "testlib.h"
#include <iostream>
#include <string>
#include <sstream> 

using namespace std;

const int MAXM = 1e5;
const long long MAXMOD = 1e9; 

int calc_period (int m) {
  int a, b, c, id; 
  a = 1; b = 1; id = 1;

  while (!(a == 0 && b == 1)) {
    c = (a + b) % m; 
    a = b; 
    b = c; 
    ++id;
  }
  return id;  
}


int n, m;
long long mod; 
int period;
vector<pair<long long, long long> > tests; 

void gen_null_tests (int t) {
  long long l, r;
  for (size_t i = 0; i < t; ++i) {
    l = rnd.next (0ll, mod);
    r = rnd.next (0ll, mod);
    if (l > r) {
      swap (l, r);
    }
    tests.push_back (make_pair (l, r));
  }
}

void gen_tests (int t) {
  long long l, len, r;
  for (size_t i = 0; i < t; ++i) {
    l = rnd.next (0ll, mod);
    len = rnd.next (0, period);
    r = l + len; 
    
    if (r < mod) {
      tests.push_back (make_pair (l, r));
    } else {
      ++t; 
    }
  }  
}

int main(int argc, char* argv[]) {
  registerGen(argc, argv, 1);

  n = atoi(argv[1]);
  n = rnd.next(n / 2, n); 
  m = atoi(argv[2]);
  mod = atoll(argv[3]);

  if (m == -1) {
    m = rnd.next (1, MAXM);
  }
  if (mod == -1) {
    mod = MAXMOD; 
  }


  period = calc_period (m);
  /* NULL */
  gen_null_tests (n / 10);
  /* Not NULL */ 
  gen_tests (n - tests.size ());

  /* Print */ 
  cout << n << " " << m << endl; 
  shuffle (tests.begin(), tests.end());
  if (tests.size () != n) {
    return 1; 
  }
  for (auto test: tests) {
    cout << test.first << " " << test.second << endl; 
  }
}