#include "testlib.h"
#include <iostream>
#include <string>

using namespace std;


const int MAXA = 1e9; 

int main(int argc, char* argv[]) {
  registerGen(argc, argv, 1);

  int n;
  n = atoi(argv[1]);
  
  cout << n << "\n"; 
  for (size_t i = 0; i < n; ++i) {
    cout << rnd.next (-MAXA, MAXA) << " ";
  }

  return 0;
}